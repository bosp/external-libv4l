
ifdef CONFIG_EXTERNAL_V4L

# Targets provided by this project
.PHONY: libv4l clean_libv4l

# Add this to the "external" target
external: libv4l
clean_external: clean_libv4l

MODULE_DIR_LIBV4L=external/optional/v4l-utils

libv4l: setup libjpeg $(BUILD_DIR)/lib/libv4l2.so.0
$(BUILD_DIR)/lib/libv4l2.so.0:
	@echo
	@echo "==== Building V4L2 Library v0.8.6 ===="
	@cd $(MODULE_DIR_LIBV4L) && \
		BOSP_CPUS=$(CPUS) \
		BOSP_HOME=$(BASE_DIR) \
		BOSP_BDIR=$(BUILD_DIR) \
		BOSP_GCC_VERSION=$(GCC_VERSION) \
		CXX=$(CXX) CC=$(CC) PATH=$(PLATFORM_PATH) \
		make -j$(CPUS) \
			PREFIX=/ \
			DESTDIR=$(BUILD_DIR) \
			CFLAGS+=-I$(BUILD_DIR)/include \
			LDFLAGS+="-L$(BUILD_DIR)/lib -Wl,-rpath-link=$(BUILD_DIR)/lib -Wl,-rpath=$(CONFIG_BOSP_RUNTIME_PATH)/lib" \
			install && \
		cp FindV4L2.cmake $(BOSP_CMAKE_MODULES)
	@echo

clean_libv4l:
	@echo "==== Clean-up V4L2 Library v0.8.6 ===="
	@cd $(MODULE_DIR_LIBV4L) && \
		BOSP_BDIR=$(BUILD_DIR) \
		make bosp_clean
	@[ ! -f $(BOSP_CMAKE_MODULES)/FindV4L2.cmake ] || \
		rm -f $(BOSP_CMAKE_MODULES)/FindV4L2.cmake
	@echo

else # CONFIG_EXTERNAL_V4L

libv4l:
	$(warning LibV4L module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_V4L

